<?php
/**
 * Template Name: Flexible Content
 *
 * @link https://developer.wordpress.org/themes/template-files-section/page-template-files/
 *
 * @package tercet
 */


require_once ('ACF/ImageSection.php');
require_once ('ACF/Investments.php');
require_once ('ACF/SectionTitle.php');
require_once ('ACF/InvestmentPlan.php');
require_once ('ACF/TextField.php');
require_once ("ACF/Localization.php");
require_once ('ACF/Separator.php');
require_once ('ACF/FooterContact.php');
require_once ('ACF/CarouselModal.php');


$image_section = new ImageSection();
$investments = new Investments();
$section_title = new SectionTitle();
$investment_plan = new InvestmentPlan();
$text_field = new TextField();
$localization = new Localization();
$separator = new Separator();
$carousel_modal = new CarouselModal();
$footer_contact = new FooterContact();


get_header();




if( have_rows('flexible') ):



    while ( have_rows('flexible') ) : the_row();
        if( get_row_layout() == 'image_section' ):
            $image_section->show_content();
        elseif ( get_row_layout() == "investments" ):
            $investments->show_content();
        elseif ( get_row_layout() == "section_title" ):
            $section_title->show_content();
        elseif ( get_row_layout() == "investment_plan" ):
            $investment_plan->show_content();
        elseif ( get_row_layout() == "text_field" ):
            $text_field->show_content();
        elseif ( get_row_layout() == "localization" ):
            $localization->show_content();
        elseif ( get_row_layout() == "separator" ):
            $separator->show_content();
        elseif ( get_row_layout() == "footer_contact" ):
            $footer_contact->show_content();
        elseif ( get_row_layout() == "modal_carousel" ):
            $carousel_modal->show_content();
        endif;

    endwhile;
else :

     echo "no layouts found";

endif;

get_footer();
