<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018-09-22
 * Time: 15:52
 */

class Investments
{
    public function show_content()
    {
        $tercet_header_phone = get_theme_mod('tercet_header_phone');
        $section_header = get_sub_field("section_header");
        ?>
        <section id="inwestycja" class="container-narrow bg-image"
                 style="background-image: url('<?php echo get_template_directory_uri() ?>/assets/src/img/images/background/map-background.png')">
            <div class="container justify-content-center d-none d-lg-block">
                <div class="pt-5 pb-5">
                    <h2 class="text-center text-uppercase text-white"><?php echo $section_header ?></h2>
                </div>
            </div>
            <div class="container d-flex justify-content-center d-lg-none">
                <div class="pt-4 pb-2">
                    <div class="phone text-center">
                        <a class="text-white i-phone"
                           href="tel:<?php echo (int)filter_var($tercet_header_phone, FILTER_SANITIZE_NUMBER_INT) ?>"><?php
                            echo $tercet_header_phone
                            ?></a>
                    </div>
                    <div class="d-flex button-box pt-3 justify-content-center">
                        <a target="_blank" href="http://ftp.napinvest.com.pl/nap/Default.aspx?Kind=14&ID=17&Code=DOM%20POD%20SZ%C3%93STK%C4%84" class="i-plan-mieszkan target-button"><span
                                class="text-white text-uppercase">Plany<br>mieszkań</span></a>
                        <a href="#" class="i-phone target-button" data-toggle="modal" data-target="#orderCall"><span class="text-white text-uppercase" href="#">Zamów<br>rozmowę</span></a>
                    </div>
                </div>
            </div>
        </section>
        <section class="container-narrow">
            <div class="container">
                <div class="investments d-sm-flex">
                <?php
                if( have_rows('investment_block') ):
                    while ( have_rows('investment_block') ) : the_row();
                    $image = get_sub_field('image_block');
                ?>
                        <div class="text-center frame">
                            <h2 class="text-blue"><?php echo the_sub_field('title_block');?></h2>
                            <div>
                                <?php echo the_sub_field('content_block');?>
                            </div>
                            <div class="img-block">
                                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
                            </div>
                        </div>
                    <?php

                    endwhile;

                endif;
                ?>
                </div>
            </div>
        </section>

        <?php
    }

}