<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018-09-15
 * Time: 21:31
 */

class ImageSection
{
    public function show_content() {
        $admin_ajax = htmlspecialchars(get_template_directory_uri() . '/inc/Custom/Contact.php');
        $tercet_header_background_image = get_theme_mod('tercet_header_background_image');
        $tercet_header_logo = get_theme_mod('tercet_header_logo');
        ?>
        <section id="image-section" class="container-narrow">
            <div class="position-relative first-section" >
                <div class="d-lg-none">
                    <img class="img-responsive"
                         src="<?php echo get_template_directory_uri() ?>/assets/src/img/images/background/osiedla-targowek-md.jpg"
                         ) alt="Osiedle targówek">
                </div>
            </div>
            <form class="contact-form header d-none d-lg-block" data-url="<?php echo $admin_ajax ?>">
                <h3 class="title">Wypełnij formularz</h3>
                <h5 class="description">skontaktuj się z biurem sprzedaży</h5>
                <div class="row">
                    <div class="col">
                        <input id="name" type="text" class="form-control" placeholder="Imię i Nazwisko*">
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <input id="email" type="email" class="form-control" placeholder="Adres email*">
                    </div>
                    <div class="col">
                        <input id="phone" type="text" class="form-control" placeholder="Telefon*">
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <textarea id="message" type="text" class="form-control" placeholder="Treść wiadomości*"></textarea>
                    </div>
                </div>
                <p class="text-blue m-0 p-0">*pola wymagane</p>
                <div class="row">
                    <div class="col">
                        <label for="hlevel" class="col-xs-12"><b>Piętro</b><span
                                    class="float-right">od <b class="level-header-min">1</b> do <b
                                        class="level-header-max">7</b></span></label>
                        <div class="col-xs-12">
                            <input id="hlevel" type="text" class="range-slider" value="" data-slider-min="1"
                                   data-slider-max="7"
                                   data-slider-step="1" data-slider-value="[1,4]"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <label for="hrooms" class="col-xs-12"><b>Liczba pokoi</b><span
                                    class="float-right">od <b class="rooms-header-min">1</b> do <b
                                        class="rooms-header-max">4</b></span></label>
                        <div class="col-xs-12">
                            <input id="hrooms" type="text" class="range-slider" value="" data-slider-min="1"
                                   data-slider-max="4"
                                   data-slider-step="1" data-slider-value="[1,4]"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <label for="harea" class="col-xs-12"><b>Powierzchnia</b><span
                                    class="float-right">od <b class="area-header-min">29.9</b> do <b
                                        class="area-header-max">94</b></span></label>
                        <div class="col-xs-12">
                            <input id="harea" type="text" class="range-slider" value="" data-slider-min="29.46"
                                   data-slider-max="96.36"
                                   data-slider-step="0.1" data-slider-value="[29.46,96.36]"/>
                        </div>
                    </div>
                </div>
                <div class="form-agreement"></div>

                <button type="submit" class="btn btn-primary">Wyślij formularz</button>

            </form>
            <div class="site-branding d-lg-none pt-2 pb-2 text-center"
                <?php
                if ($tercet_header_background_image) :
                    echo 'style="background-image: url(\' ' . $tercet_header_background_image . ' \');
                background-size: cover;"';
                endif; ?>>
                <img class="img-responsive" src="<?php
                if ($tercet_header_logo) :
                    echo $tercet_header_logo;
                else:
                    echo get_template_directory_uri() . '/assets/src/img/images/LOGO_NAPOLLO.png';
                endif; ?>" alt="Logo Napollo">
            </div><!-- .site-branding -->
        </section>
              <?php
    }
}