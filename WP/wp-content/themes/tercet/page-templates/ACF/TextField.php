<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 20.07.2018
 * Time: 14:55
 */

class TextField {
	public function show_content() {
	    $choice  = get_sub_field('container_width');
	    $container = $choice === "Normal" ? "container": "container-narrow";
        echo "<div class=\"$container\">";

		while ( have_rows( 'text_block' ) ) : the_row();
			$mobile  = !empty(get_sub_field( 'mobile' )) ? get_sub_field( 'mobile' ) : 12;
			$tablet  = !empty(get_sub_field( 'tablet' )) ? get_sub_field( 'tablet' ) : 12;
			$desktop = !empty(get_sub_field( 'desktop' )) ? get_sub_field( 'desktop' ) : 12;
			$text = !empty(get_sub_field( 'text' )) ? get_sub_field( 'text' ): "";
			echo "<div class=\"col-xs-$mobile col-md-$tablet col-lg-$desktop\">";
				echo $text;
			echo "</div>";
		endwhile;
        echo "</div>";

	}

}

