<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018-09-23
 * Time: 22:10
 */

class CarouselModal
{
    private static $id = 0;

    public function show_content()
    {
        $id = self::$id++;
        ?>
        <div class='container pd-container carousel-container'>
            <div class='row pd-row'>
                <?php
                if (have_rows('container')):
                    $i = 0;
                    while (have_rows('container')) : the_row();
                        ?>
                        <div class='pd-area '>
                            <a href='#gallery-<?php echo $id . '-' . $i ?>' data-slide-to='0'>
                                <?php
                                $target_text = 'data-toggle="modal" data-target="#showCarouselModal-' . $id . '-' . $i . '"';
                                $target = get_sub_field('images_carousel') ? $target_text : '';
                                ?>
                                <div class='effect-bubba pd-rec' <?php echo $target ?>>
                                    <img class='img-reponsive'
                                         src='<?php echo get_sub_field('image')['url'] ?>'
                                         alt="<?php echo get_sub_field('image')['alt'] ?>"/>
                                    <figcaption></figcaption>
                                </div>
                            </a>
                            <div class="text">
                                <h3 class='text-uppercase text-blue'><?php echo get_sub_field('title') ?></h3>
                            </div>
                            <div class="separator mb-3"></div>
                        </div>
                        <?php
                        $i++;
                    endwhile;
                endif;
                ?>

            </div>
        </div>
        <!-- modal -->
        <!--show product modal-->
        <?php
        if (have_rows('container')):
            $i = 0;
            while (have_rows('container')) : the_row();
                ?>
                <div id="showCarouselModal-<?php echo $id . '-' . $i ?>" class="modal fade show-image">
                    <div class="modal-dialog">
                        <div class="modal-content" >

                            <!--begin carousel-->
                            <div class="close text-white text-right" data-dismiss="modal"
                                 aria-hidden="true">&times;
                            </div>
                            <div id="gallery-<?php echo $id . '-' . $i ?>" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <?php
                                    if (have_rows('images_carousel')):
                                        $j = 0;
                                        while (have_rows('images_carousel')) : the_row();
                                        $active = $j == 0 ? ' active' : '';
                                            ?>
                                            <div class="carousel-item <?php echo $active ?>">
                                                <img class="d-block m-auto"
                                                     src="<?php echo get_sub_field('image')['url']; ?>"
                                                     alt="<?php echo get_sub_field('image')['alt']; ?>">
                                            </div>
                                        <?php
                                        $j++;
                                        endwhile;
                                    endif;
                                    ?>
                                </div>
                                <a class="carousel-control-prev" href="#gallery-<?php echo $id . '-' . $i ?>"
                                   role="button"
                                   data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#gallery-<?php echo $id . '-' . $i ?>"
                                   role="button"
                                   data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <!--end modal-->

                <?php
                $i++;
            endwhile;
        endif;
    }

}