<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018-09-23
 * Time: 21:00
 */

class Separator
{
    public function show_content()
    {
        ?>
            <section class="pt-10 pb-10 pb-md-5 pt-md-5">
                <div class="container-narrow separator-section text-center">
                    <img src="/wp-content/themes/tercet/assets/src/img/images/background/squred-line.png" alt="separator">
                </div>
            </section>
        <?php
    }
}