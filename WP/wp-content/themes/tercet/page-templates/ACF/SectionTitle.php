<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018-09-22
 * Time: 17:40
 */

class SectionTitle
{
    public function show_content(){
        ?>
        <section id="<?php echo get_sub_field('id'); ?>" class="container-narrow bg-image"
                 style="background-image: url('<?php echo get_template_directory_uri() ?>/assets/src/img/images/background/map-background.png')">
            <div class="container justify-content-center">
                <div class="pt-5 pb-5">
                    <h2 class="text-center text-uppercase text-white">
                        <?php echo get_sub_field('section_title'); ?>
                    </h2>
                </div>
            </div>
        </section>
    <?php
    }
}