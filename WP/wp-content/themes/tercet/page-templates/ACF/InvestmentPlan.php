<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018-09-22
 * Time: 19:39
 */

class InvestmentPlan
{
    public function show_content()
    {
        ?>
        <section class="container-narrow investments-plan">
            <div class="container">
               <div class="row">
                   <div class="col-xs-12 col-md-8"><?php echo get_sub_field('left_content') ?></div>
                   <div class="col-xs-12 col-md-4 pr-2 pl-md-2 pl-3"><?php echo get_sub_field('right_content') ?></div>
               </div>
            </div>
        </section>

        <?php
    }

}