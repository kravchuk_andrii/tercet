<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018-09-23
 * Time: 21:13
 */

class FooterContact
{
    public function show_content()
    {
        $admin_ajax = htmlspecialchars(get_template_directory_uri() . '/inc/Custom/Contact.php');
        ?>
        <section id="kontakt" class="container-narrow">
            <div class="justify-content-center contact position-relative"
                 style="background-image: url('<?php echo get_template_directory_uri() ?>/assets/src/img/images/background/investycja-targowek.png')">
                <div class="row">
                    <div class="col-md-4 col-xs-12 text-center contact-data background-white">
                        <img class="d-none d-md-inline"
                             src="<?php echo get_template_directory_uri() ?>/assets/src/img/images/tercet-footer.png">
                        <img class="d-md-none"
                             src="<?php echo get_template_directory_uri() ?>/assets/src/img/images/tercet-footer-mobile.png">
                        <h5 class="pt-7 text-pink"><b>BIURO SPRZEDAŻY</b></h5>
                        <p class="h5 pt-2 pb-4 text-blue"><b>ul. Poborzańska 39, lok. 1<br>
                                03-368 Warszawa</b></p>
                        <div class="separator"></div>
                        <h5 class="pt-2 text-pink telephone"> +48 <b>22 274 18 09</b></h5>
                        <p class="h5 pb-2 text-blue"><b>mieszkania@osiedletercet.pl</b></p>
                        <div class="separator"></div>
                        <h5 class="pt-4 text-pink"><b>Godziny pracy biura</b></h5>
                        <p class="h5 pt-2 pb-4 text-blue">pn.: <b>10:00 - 18:00</b><br>
                            wt. - pt.: <b>9:00 - 17:00</b><br>
                            sob.: <b>10:00 - 15:00</b></p>
                    </div>
                    <form class="contact-form footer"data-url="<?php echo $admin_ajax ?>">
                        <h3 class="title">Wypełnij formularz</h3>
                        <h5 class="description">skontaktuj się z biurem sprzedaży</h5>
                        <div class="row">
                            <div class="col">
                                <input id="name" type="text" class="form-control" placeholder="Imię i Nazwisko*">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <input id="email" type="email" class="form-control" placeholder="Adres email*">
                            </div>
                            <div class="col">
                                <input id="phone" type="text" class="form-control" placeholder="Telefon*">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <textarea id="message" type="text" class="form-control" placeholder="Treść wiadomości*"></textarea>
                            </div>
                        </div>
                        <p class="text-blue m-0 p-0">*pola wymagane</p>
                        <div class="row">
                            <div class="col">
                                <label for="flevel" class="col-xs-12"><b>Piętro</b><span
                                        class="float-right">od <b class="level-footer-min">1</b> do <b
                                            class="level-footer-max">7</b></span></label>
                                <div class="col-xs-12">
                                    <input id="flevel" type="text" class="range-slider" value="" data-slider-min="1"
                                           data-slider-max="7"
                                           data-slider-step="1" data-slider-value="[1,4]"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label for="frooms" class="col-xs-12"><b>Liczba pokoi</b><span
                                        class="float-right">od <b class="rooms-footer-min">1</b> do <b
                                            class="rooms-footer-max">4</b></span></label>
                                <div class="col-xs-12">
                                    <input id="frooms" type="text" class="range-slider" value="" data-slider-min="1"
                                           data-slider-max="4"
                                           data-slider-step="1" data-slider-value="[1,4]"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label for="farea" class="col-xs-12"><b>Powierzchnia</b><span
                                        class="float-right">od <b class="area-footer-min">29.9</b> do <b
                                            class="area-footer-max">94</b></span></label>
                                <div class="col-xs-12">
                                    <input id="farea" type="text" class="range-slider" value="" data-slider-min="29.46"
                                           data-slider-max="96.36"
                                           data-slider-step="0.1" data-slider-value="[29.46,96.36]"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-agreement"></div>
                        <button type="submit" class="btn btn-primary">Wyślij formularz</button>

                    </form>
                    <div class="col-xs-12 d-md-none">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/src/img/images/background/investycja-targowek-mob.png"
                             alt="investycja targówek">
                    </div>
                </div>
            </div>
        </section>
        </div>
        <?php
    }

}