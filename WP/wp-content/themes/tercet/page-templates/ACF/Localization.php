<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018-09-23
 * Time: 20:05
 */

class Localization
{
    public function show_content()
    {
        $mapa = get_sub_field('mapa');
        ?>
        <section id="localization">
            <div class="container">
                <div class="row">
                    <div class="map-images">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/src/img/images/lokalizacja/TERCET_MAPA/MAPA.png" alt="mapa z logo">
                        <?php
                        $dirname = "wp-content/themes/tercet/assets/src/img/images/lokalizacja/TERCET_MAPA/";
                        $images = glob($dirname. "*.png" );
                        foreach($images as $image) {
                            echo '<img class="image-layer visible" data-image="' .  basename($image, '.png') .'" src="'.$image.'" />';
                        }
                        ?>
                    </div>
                    <div class="col-xl-3">
                        <div class="text-center  map-container">
                            <p class="text-blue map-item active" data-element="renova"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/assets/src/img/images/lokalizacja/galeria.png" alt="Galeria" ><br>
                                Galeria<br>
                                Renova</p>
                            <p class="text-blue map-item active " data-element="tercet"><img class="img-responsive " src="<?php echo get_template_directory_uri() ?>/assets/src/img/images/lokalizacja/tercet.png" alt="tercet" ></p>
                            <p class="text-blue map-item active " data-element="sklepy"><img class="img-responsive"  src="<?php echo get_template_directory_uri() ?>/assets/src/img/images/lokalizacja/sklepy.png" alt="sklepy"><br>
                                Sklepy</p>
                            <p class="text-blue map-item active " data-element="autobusy"><img class="img-responsive " src="<?php echo get_template_directory_uri() ?>/assets/src/img/images/lokalizacja/autobusy.png" alt="Autobusy"><br>
                                Autobusy</p>
                            <p class="text-blue map-item active " data-element="tramwaje"><img class="img-responsive " src="<?php echo get_template_directory_uri() ?>/assets/src/img/images/lokalizacja/tramwaje.png" alt="tramwaje"><br>
                                Tramwaje</p>
                            <p class="text-blue map-item active " data-element="metro"><img class="img-responsive " src="<?php echo get_template_directory_uri() ?>/assets/src/img/images/lokalizacja/metro.png" alt="metro"><br>
                                II linia metra<br>
                                planowana<br>
                                2020&nbsp;r.</p>
                            <p class="text-blue map-item active " data-element="restauracje"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/assets/src/img/images/lokalizacja/restauracje.png" alt="Restauracje"><br>
                                Restauracje<br>
                                Kawiarnie</p>
                            <p class="text-blue map-item active " data-element="szpital"><img class="img-responsive"  src="<?php echo get_template_directory_uri() ?>/assets/src/img/images/lokalizacja/szpital.png" alt="szpital"><br>
                                Mazowiecki<br>
                                Szpital<br>
                                Bródnowski</p>
                            <p class="text-blue map-item active " data-element="urzad"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/assets/src/img/images/lokalizacja/urzad.png" alt="urząd skarbowy" ><br>
                                Urząd<br>
                                Dzielnicy<br>
                                Targówek</p>
                            <p class="text-blue map-item active " data-element="zlobki"><img class="img-responsive "  src="<?php echo get_template_directory_uri() ?>/assets/src/img/images/lokalizacja/zlobki.png" alt="zlobki"><br>
                                Żłobki<br>
                                Przedszkola</p>
                            <p class="text-blue map-item active " data-element="szkoly"><img class="img-responsive " src="<?php echo get_template_directory_uri() ?>/assets/src/img/images/lokalizacja/szkoly.png" alt="szkoly"><br>
                                Szkoły</p>
                            <p class="text-blue map-item active " data-element="rowery"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/assets/src/img/images/lokalizacja/stacje-rowerowe.png"  alt="stacje rowerowe"><br>
                                Stacje<br>
                                Rowerowe</p>`
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php
    }

}