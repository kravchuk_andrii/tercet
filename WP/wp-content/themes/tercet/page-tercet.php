<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018-07-18
 * Time: 21:13
 */
get_header();
$admin_ajax = htmlspecialchars(get_template_directory_uri() . '/inc/Custom/Contact.php');
$tercet_header_background_image = get_theme_mod('tercet_header_background_image');
$tercet_header_phone = get_theme_mod('tercet_header_phone');
$tercet_header_logo = get_theme_mod('tercet_header_logo');
?>
<div id="tercet-page">
    <section id="image-section" class="container-narrow">
        <div class="position-relative first-section" >
            <div class="d-lg-none">
                <img class="img-responsive"
                     src="<?php echo get_template_directory_uri() ?>/assets/src/img/images/background/osiedla-targowek-md.jpg"
                     ) alt="Osiedle targówek">
            </div>
        </div>
        <form class="contact-form header d-none d-lg-block" data-url="<?php echo $admin_ajax ?>">
            <h3 class="title">Wypełnij formularz</h3>
            <h5 class="description">skontaktuj się z biurem sprzedaży</h5>
            <div class="row">
                <div class="col">
                    <input id="name" type="text" class="form-control" placeholder="Imię i Nazwisko*">
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <input id="email" type="email" class="form-control" placeholder="Adres email*">
                </div>
                <div class="col">
                    <input id="phone" type="text" class="form-control" placeholder="Telefon*">
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <textarea id="message" type="text" class="form-control" placeholder="Treść wiadomości*"></textarea>
                </div>
            </div>
            <p class="text-blue m-0 p-0">*pola wymagane</p>
            <div class="row">
                <div class="col">
                    <label for="hlevel" class="col-xs-12"><b>Piętro</b><span
                                class="float-right">od <b class="level-header-min">1</b> do <b
                                    class="level-header-max">7</b></span></label>
                    <div class="col-xs-12">
                        <input id="hlevel" type="text" class="range-slider" value="" data-slider-min="1"
                               data-slider-max="7"
                               data-slider-step="1" data-slider-value="[1,4]"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label for="hrooms" class="col-xs-12"><b>Liczba pokoi</b><span
                                class="float-right">od <b class="rooms-header-min">1</b> do <b
                                    class="rooms-header-max">4</b></span></label>
                    <div class="col-xs-12">
                        <input id="hrooms" type="text" class="range-slider" value="" data-slider-min="1"
                               data-slider-max="4"
                               data-slider-step="1" data-slider-value="[1,4]"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label for="harea" class="col-xs-12"><b>Powierzchnia</b><span
                                class="float-right">od <b class="area-header-min">29.9</b> do <b
                                    class="area-header-max">94</b></span></label>
                    <div class="col-xs-12">
                        <input id="harea" type="text" class="range-slider" value="" data-slider-min="29.46"
                               data-slider-max="96.36"
                               data-slider-step="0.1" data-slider-value="[29.46,96.36]"/>
                    </div>
                </div>
            </div>
            <div class="form-agreement"></div>

            <button type="submit" class="btn btn-primary">Wyślij formularz</button>

        </form>
        <div class="site-branding d-lg-none pt-2 pb-2 text-center"
            <?php
            if ($tercet_header_background_image) :
                echo 'style="background-image: url(\' ' . $tercet_header_background_image . ' \');
                background-size: cover;"';
            endif; ?>>
            <img class="img-responsive" src="<?php
            if ($tercet_header_logo) :
                echo $tercet_header_logo;
            else:
                echo get_template_directory_uri() . '/assets/src/img/images/LOGO_NAPOLLO.png';
            endif; ?>" alt="Logo Napollo">
        </div><!-- .site-branding -->
    </section>
    <section id="inwestycja" class="container-narrow bg-image"
             style="background-image: url('<?php echo get_template_directory_uri() ?>/assets/src/img/images/background/map-background.png')">
        <div class="container justify-content-center d-none d-lg-block">
            <div class="pt-5 pb-5">
                <h2 class="text-center text-uppercase text-white">atuty inwestycji</h2>
            </div>
        </div>
        <div class="container d-flex justify-content-center d-lg-none">
            <div class="pt-4 pb-2">
                <div class="phone i-phone">
                    <a class="text-white"
                       href="tel:<?php echo (int)filter_var($tercet_header_phone, FILTER_SANITIZE_NUMBER_INT) ?>"><?php
                        echo $tercet_header_phone
                        ?></a>
                </div>
                <div class="d-flex button-box pt-3 justify-content-center">
                    <a target="_blank" href="http://ftp.napinvest.com.pl/nap/Default.aspx?Kind=14&ID=17&Code=DOM%20POD%20SZ%C3%93STK%C4%84" class="i-plan-mieszkan target-button"><span
                                class="text-white text-uppercase">Plany<br>mieszkań</span></a>
                    <a href="#" class="i-phone target-button" data-toggle="modal" data-target="#orderCall"><span class="text-white text-uppercase" href="#">Zamów<br>rozmowę</span></a>
                </div>
            </div>
        </div>
    </section>
    <section class="container-narrow">
        <div class="container">
            <div class="investments d-sm-flex">
                <div class="text-center frame">
                    <h2 class="text-blue">ZAUROCZENI<br>TARGÓWKIEM</h2>
                    <div>
                        <p>Dużo zieleni, spokojna okolica, dobra komunikacja i infrastruktura to największe
                            atuty warszawskiego Targówka.<br>
                            Dzielnica dynamicznie się rozwija. Jest świetnie skomunikowana z centrum miasta i
                            innymi częściami Warszawy. Ma doskonale rozwiniętą infrastrukturę – liczne sklepy,
                            punkty usługowe, centra handlowe, placówki edukacyjne. Tereny zielone stanowią
                            jedną trzecią jej powierzchni i zapewniają mieszkańcom liczne atrakcje sportowe,
                            kulturalne i artystyczne a także lepszą jakość powietrza.<br>
                            Osiedle Tercet to nowoczesna inwestycja zaprojektowana z myślą o osobach
                            poszukujących przemyślanej estetycznie i funkcjonalnie przestrzeni życiowej.
                            Składa się z trzech budynków położonych wzdłuż ulicy Balkonowej, wycofanej w
                            stosunku do głównych arterii dzielnicy, w odległości zaledwie 10 minut spacerem od
                            Parku Bródnowskiego.</p>
                    </div>
                    <div class="img-block">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/src/img/images/icon-png/architektura.png">
                    </div>
                </div>
                <div class="text-center frame">
                    <h2 class="text-blue">POSZUKUJĄCY<br>KOMFORTU</h2>
                    <div>
                        <p>Tercet to oryginalny projekt na mapie Targówka. Powstał z myślą o tych, którzy
                            doceniają dobry design i szukają w codziennym życiu równowagi. Nowoczesna
                            zabudowa, szeroki wybór lokali oraz istniejąca infrastruktura handlowo-usługowa
                            sprawiają, że żyje się tu komfortowo.<br>
                            W ofercie Tercetu znajdują się wygodne i funkcjonalne kawalerki oraz 2-, 3- i 4-
                            pokojowe mieszkania o powierzchni od 29,5 m² do 96,4 m². To osiedle o otwartym
                            charakterze ze starannie zaprojektowaną przestrzenią wspólną - placem zabaw dla
                            dzieci i terenem zielonym z małą architekturą.<br>
                            Dzięki stonowanej kolorystyce fasad i powtarzalnemu rytmowi ozdobnych motywów
                            na balustradach balkonów trzy eleganckie budynki tworzą spójny stylistycznie
                            kompleks. W części podziemnej do dyspozycji mieszkańców są miejsca parkingowe i
                            komórki lokatorskie. Na parterze budynków zaplanowane są lokale handlowe i
                            usługowe.</p>
                    </div>
                    <div class="img-block">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/src/img/images/icon-png/kanapa.png">
                    </div>
                </div>
                <div class="text-center frame">
                    <h2 class="text-blue">ZACHWYCENI<br>LOKALIZACJĄ</h2>
                    <div>
                        <p>Dom to nie tylko dopasowane do naszych potrzeb wnętrze. To również przyjazne
                            otoczenie, na które składają się dobrze rozwinięte komunikacja i infrastruktura
                            oraz dostępność oferty kulturalnej i edukacyjnej.<br>
                            Na terenie Targówka działa kilkadziesiąt przedszkoli i szkół podstawowych, licea,
                            technika, szkoły policealne oraz prywatna wyższa uczelnia. Do innych części miasta
                            łatwo stąd dostać się samochodem, dzięki bliskości ważnych miejskich arterii
                            takich jak Trasa Toruńska, ulice Radzymińska i Głębocka. Korzystający z
                            komunikacji miejskiej mają do dyspozycji kilkanaście linii autobusowych, a
                            rowerzyści ponad 13 km ścieżek rowerowych.<br>
                            Zieloną część Targówka stanowią Park leśny Bródno, Park Bródnowski, Park im.
                            Stefana Wiecheckiego „Wiecha” oraz liczne zielone skwery i ogródki działkowe.
                            Park Bródnowski to jednocześnie miejsce spotkań ze sztuką. Na jego terenie działa
                            Park Rzeźby – inicjatywa warszawskiego Muzeum Sztuki Nowoczesnej.
                        </p>
                    </div>
                    <div class="img-block">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/src/img/images/icon-png/map-mark.png">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="container-narrow bg-image"
             style="background-image: url('<?php echo get_template_directory_uri() ?>/assets/src/img/images/background/map-background.png')">
        <div class="container justify-content-center">
            <div class="pt-5 pb-5">
                <h2 class="text-center text-uppercase text-white">lokalizacja</h2>
            </div>
        </div>
    </section>
    <section class="container-narrow">
        <div class="container justify-content-center">
            <div class="col-xs-12">
                <div id="map"></div>
                <div class="switcher-container">
                    <div class="switcher-icon" data-google="restaurant">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/src/img/lokalizacja/restauracje_inactive.svg">
                        <p>Restauracje<br>
                            Kawiarnie</p>
                    </div>
                    <div class="switcher-icon" data-google="supermarket">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/src/img/lokalizacja/sklep_inactive.svg">
                        <p>Sklepy</p>
                    </div>
                    <div class="switcher-icon" data-google="transit_station">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/src/img/lokalizacja/bus_inactive.svg">
                        <p>Autobusy</p>
                    </div>
                    <div class="switcher-icon active" data-google="light_rail_station">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/src/img/lokalizacja/tram_active.svg">
                        <p>Tramwaje</p>
                    </div>
                    <div class="switcher-icon" data-google="subway_station">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/src/img/lokalizacja/metro_inactive.svg">
                        <p>Metro</p>
                    </div>
                    <div class="switcher-icon" data-google="hospital">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/src/img/lokalizacja/szpital_inactive.svg">
                        <p>Mazowiecki<br>
                            Szpital<br>
                            Bródnowski</p>
                    </div>
                    <div class="switcher-icon" data-google="local_government_office">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/src/img/lokalizacja/urzad_inactive.svg">
                        <p>Urząd<br>Dzielnicy&nbsp;Targówek<br>m.&nbsp;st.&nbsp;Warszawy</p>
                    </div>
                    <div class="switcher-icon" data-google="bus_station">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/src/img/lokalizacja/zlobki_inactive.svg">
                        <p>Żłobki<br>
                            Przedszkola</p>
                    </div>
                    <div class="switcher-icon" data-google="school">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/src/img/lokalizacja/szkoly_inactive.svg">
                        <p>Szkoły</p>
                    </div>
                    <div class="switcher-icon" data-google="bicycle_store">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/src/img/lokalizacja/rower_inactive.svg">
                        <p>Stacje<br>
                            Rowerowe</p>
                    </div>

                </div>
            </div>
            <script async defer
                    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBYv6fwGpwYFaiHJYqno7ALU-zkaMcYVJo&libraries=places"
                    type="text/javascript">
            </script>
        </div>
    </section>

    <section class="container-narrow">
        <div class="justify-content-center contact position-relative"
             style="background-image: url('<?php echo get_template_directory_uri() ?>/assets/src/img/images/background/investycja-targowek.png')">
            <div class="row">
                <div class="col-md-4 col-xs-12 text-center contact-data background-white">
                    <img class="d-none d-md-inline"
                         src="<?php echo get_template_directory_uri() ?>/assets/src/img/images/tercet-footer.png">
                    <img class="d-md-none"
                         src="<?php echo get_template_directory_uri() ?>/assets/src/img/images/tercet-footer-mobile.png">
                    <h5 class="pt-7 text-pink"><b>BIURO SPRZEDAŻY</b></h5>
                    <p class="h5 pt-2 pb-4 text-blue"><b>ul. Poborzańska 39, lok. 1<br>
                            03-368 Warszawa</b></p>
                    <div class="separator"></div>
                    <h5 class="pt-2 text-pink telephone"> +48 <b>22 274 18 09</b></h5>
                    <p class="h5 pb-2 text-blue"><b>mieszkania@osiedletercet.pl</b></p>
                    <div class="separator"></div>
                    <h5 class="pt-4 text-pink"><b>Godziny pracy biura</b></h5>
                    <p class="h5 pt-2 pb-4 text-blue">pn.: <b>10:00 - 18:00</b><br>
                        wt. - pt.: <b>9:00 - 17:00</b><br>
                        sob.: <b>10:00 - 15:00</b></p>
                </div>
                <form class="contact-form footer"data-url="<?php echo $admin_ajax ?>">
                    <h3 class="title">Wypełnij formularz</h3>
                    <h5 class="description">skontaktuj się z biurem sprzedaży</h5>
                    <div class="row">
                        <div class="col">
                            <input id="name" type="text" class="form-control" placeholder="Imię i Nazwisko*">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <input id="email" type="email" class="form-control" placeholder="Adres email*">
                        </div>
                        <div class="col">
                            <input id="phone" type="text" class="form-control" placeholder="Telefon*">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <textarea id="message" type="text" class="form-control" placeholder="Treść wiadomości*"></textarea>
                        </div>
                    </div>
                    <p class="text-blue m-0 p-0">*pola wymagane</p>
                    <div class="row">
                        <div class="col">
                            <label for="flevel" class="col-xs-12"><b>Piętro</b><span
                                        class="float-right">od <b class="level-footer-min">1</b> do <b
                                            class="level-footer-max">7</b></span></label>
                            <div class="col-xs-12">
                                <input id="flevel" type="text" class="range-slider" value="" data-slider-min="1"
                                       data-slider-max="7"
                                       data-slider-step="1" data-slider-value="[1,4]"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label for="frooms" class="col-xs-12"><b>Liczba pokoi</b><span
                                        class="float-right">od <b class="rooms-footer-min">1</b> do <b
                                            class="rooms-footer-max">4</b></span></label>
                            <div class="col-xs-12">
                                <input id="frooms" type="text" class="range-slider" value="" data-slider-min="1"
                                       data-slider-max="4"
                                       data-slider-step="1" data-slider-value="[1,4]"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label for="farea" class="col-xs-12"><b>Powierzchnia</b><span
                                        class="float-right">od <b class="area-footer-min">29.9</b> do <b
                                            class="area-footer-max">94</b></span></label>
                            <div class="col-xs-12">
                                <input id="farea" type="text" class="range-slider" value="" data-slider-min="29.46"
                                       data-slider-max="96.36"
                                       data-slider-step="0.1" data-slider-value="[29.46,96.36]"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-agreement"></div>
                    <button type="submit" class="btn btn-primary">Wyślij formularz</button>

                </form>
                <div class="col-xs-12 d-md-none">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/src/img/images/background/investycja-targowek-mob.png"
                         alt="investycja targówek">
                </div>
            </div>
        </div>
    </section>
</div>



<?php
get_footer();
?>

