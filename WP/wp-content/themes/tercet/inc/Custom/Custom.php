<?php

namespace tercet\Custom;
use tercet\Custom\Contact;

/**
 * Custom
 * use it to write your custom functions.
 */
class Custom
{
	/**
     * register default hooks and actions for WordPress
     * @return
     */
	public function register()
	{
		add_action( 'init', array( $this, 'custom_post_type' ) );
		add_action( 'after_switch_theme', array( $this, 'rewrite_flush' ));
        add_action('wp_ajax_tercet_save_user_contact_form', array( $this, 'tercet_save_contact' ));
//        add_action('wp_ajax_nopriv_tercet_save_user_contact_form', array( $this, 'tercet_save_contact' ));
	}

	/**
	 * Create Custom Post Types
	 * @return The registered post type object, or an error object
	 */
	public function custom_post_type() 
	{
		$labels = array(
			'name'               => _x( 'Books', 'post type general name', 'tercet' ),
			'singular_name'      => _x( 'Book', 'post type singular name', 'tercet' ),
			'menu_name'          => _x( 'Books', 'admin menu', 'tercet' ),
			'name_admin_bar'     => _x( 'Book', 'add new on admin bar', 'tercet' ),
			'add_new'            => _x( 'Add New', 'book', 'tercet' ),
			'add_new_item'       => __( 'Add New Book', 'tercet' ),
			'new_item'           => __( 'New Book', 'tercet' ),
			'edit_item'          => __( 'Edit Book', 'tercet' ),
			'view_item'          => __( 'View Book', 'tercet' ),
			'view_items'         => __( 'View Books', 'tercet' ),
			'all_items'          => __( 'All Books', 'tercet' ),
			'search_items'       => __( 'Search Books', 'tercet' ),
			'parent_item_colon'  => __( 'Parent Books:', 'tercet' ),
			'not_found'          => __( 'No books found.', 'tercet' ),
			'not_found_in_trash' => __( 'No books found in Trash.', 'tercet' )
		);

		$args = array(
			'labels'             => $labels,
			'description'        => __( 'Description.', 'tercet' ),
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_icon'          => 'dashicons-book-alt',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'book' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => 5, // below post
			'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
		);

		register_post_type( 'book', $args );
	}

	/**
	 * Flush Rewrite on CPT activation
	 * @return empty
	 */
	public function rewrite_flush() 
	{   
		// call the CPT init function
		$this->custom_post_type();

		// Flush the rewrite rules only on theme activation
		flush_rewrite_rules();
	}

    /**
     * extend upload type
     * @return mixed
     */
    public function tercet_save_contact(){

        echo "hello";
        die();
        $contact = new Contact();
        $contact->tercet_save_contact();
    }

//    public function my_myme_types() {
//
//        $mime_types['svg'] = 'image/svg+xml';     // Adding .svg extension
//
//        return $mime_types;
//    }
}
