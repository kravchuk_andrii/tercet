<?php

namespace tercet\Custom;

/**
 * Extras.
 */
class Extras
{
	/**
     * register default hooks and actions for WordPress
     * @return
     */
	public function register()
	{
		add_filter( 'body_class', array( $this, 'body_class' ) );
        add_filter( 'upload_size_limit', array( $this, 'filter_site_upload_size_limit' ) );
	}

	public function body_class( $classes )
	{

		// Adds a class of group-blog to blogs with more than 1 published author.
		if ( is_multi_author() ) {
			$classes[] = 'group-blog';
		}
		// Adds a class of hfeed to non-singular pages.
		if ( ! is_singular() ) {
			$classes[] = 'hfeed';
		}

		return $classes;
	}

    public function filter_site_upload_size_limit( $size ) {
        if ( current_user_can( 'manage_options' ) ) {
            // 60 MB.
            $size = 1024 * 60000;
        }else{
            $size = 1024 * 2000;
        }
        return $size;
    }
}
