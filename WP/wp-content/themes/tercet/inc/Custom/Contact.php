<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018-08-18
 * Time: 21:56
 */

namespace tercet\Custom;

$contact = new Contact();
$contact->tercet_save_contact();
die();

class Contact
{

    public function tercet_save_contact()
    {

        if (isset($_POST['action']) && ($_POST['action'] === 'tercet_save_user_contact_form')) {
            $name = isset($_POST['name']) ? strip_tags($_POST['name']) : "";
            $email =  isset($_POST['name']) ? strip_tags($_POST['email']) : "";
            $phone =  isset($_POST['name']) ? strip_tags($_POST['phone']) : "";
            $message = isset($_POST['name']) ? strip_tags($_POST['message']) : "";

            if(isset($_POST['name']) ){
                $level = isset($_POST['hlevel']) ? strip_tags($_POST['hlevel']) : strip_tags($_POST['flevel']);
                $rooms = isset($_POST['hrooms']) ? strip_tags($_POST['hrooms']) : strip_tags($_POST['frooms']);
                $area = isset($_POST['harea']) ? strip_tags($_POST['harea']) : strip_tags($_POST['farea']);
            }

            if (!isset($_POST['agreement1'])) {
               echo 'Zgoda';
                return;
            } else if (isset($_POST['agreement2'])) {
                $agree1 = 0;
            } else if (isset($_POST['agreement3'])) {
                $agree1 = 0;
            }

            if (filter_var($email, FILTER_VALIDATE_EMAIL) && $this->check($name) && preg_match("/^([0-9]|\+|\(|\))*$/", $phone)) {
                $this->sendContactByMail(trim($name), trim($email), trim($phone),trim($message),trim($level), trim($rooms), trim($area));
                echo "sent";
            } else if( preg_match("/^([0-9]|\+|\(|\))*$/", $phone)) {
                $this->sendPhoneByMail($phone);
                echo "sent";
            } else {
                echo "contact form NOT sent";
            }
            die();
        }
    }
    public function check($str) {
        if(strlen($str) >= 3) { return true; } else { return false; }
    }
    public function fixChars($str) {
        return htmlspecialchars($str, ENT_QUOTES);
    }
    public function sendContactByMail($name, $email, $phone, $message, $level, $rooms, $area)
    {

        $subjectPrefix = 'Kontakt klienta';
        $emailTo = "andrii@grr.la";

        $errors = array(); // array to hold validation errors
        $data = array(); // array to pass back data

        if (filter_var($email, FILTER_VALIDATE_EMAIL) && (strlen($name) >= 3) && (strlen($phone) >= 3)) {

            $name = stripslashes(trim($_POST['name']));
            $phone = stripslashes(trim($_POST['phone']));
            $email = stripslashes(trim($_POST['email']));
            $message = stripslashes(trim($_POST['message']));


            if (empty($name)) {
                $errors['name'] = 'Imię jest wymagane';
            }

            if (empty($phone)) {
                $errors['phone'] = 'Telefon jest wymagany';
            }

            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $errors['email'] = 'Email jest nie poprawny';
            }
            if (empty($message)) {
                $errors['message'] = 'To pole jest wymagane';
            }


            // if there are any errors in our errors array, return a success boolean or false
            if (!empty($errors)) {
                $data['success'] = false;
                $data['errors'] = $errors;
            } else {
                $subject = "Message from $subjectPrefix";
                $body = '
                   Name: ' . $name . '
                   Email: ' . $email . '
                   Telefon: ' . $phone . '
                   Message: ' . nl2br($message) . '
                   Piętro: ' . $level . '
                   Liczba pokoi: ' . $rooms . '
                   Powierzchnia: ' . $area ;

                $headers = "MIME-Version: 1.1" . PHP_EOL;
                $headers .= "Content-type: text/html; charset=utf-8" . PHP_EOL;
                $headers .= "Content-Transfer-Encoding: 8bit" . PHP_EOL;
                $headers .= "Date: " . date('r', $_SERVER['REQUEST_TIME']) . PHP_EOL;
                $headers .= "Message-ID: <" . $_SERVER['REQUEST_TIME'] . md5($_SERVER['REQUEST_TIME']) . '@' . $_SERVER['SERVER_NAME'] . '>' . PHP_EOL;
                $headers .= "From: " . "=?UTF-8?B?" . base64_encode($name) . "?=" . " <$email> " . PHP_EOL;
                $headers .= "Return-Path: $emailTo" . PHP_EOL;
                $headers .= "Reply-To: $email" . PHP_EOL;
                $headers .= "X-Mailer: PHP/" . phpversion() . PHP_EOL;
//                $headers .= "X-Originating-IP: " . $_SERVER['SERVER_ADDR'] . PHP_EOL;
                $headers .= "X-Originating-IP: " . $_SERVER['REMOTE_ADDR'] . PHP_EOL;

//                mail($emailTo, "=?utf-8?B?" . base64_encode($subject) . "?=", $body, $headers);
                mail($emailTo, 'subject', 'message', $headers);
                $data['success'] = true;
                $data['confirmation'] = 'Witamy Twoja wiadomośc została wysłana';
            }

            // return all our data to an AJAX call
//            echo json_encode($data);
        }
    }
    public function sendPhoneByMail( $phone )
    {

        $subjectPrefix = 'Kontakt klienta';
        $emailTo = "andrii@grr.la";

        $errors = array(); // array to hold validation errors
        $data = array(); // array to pass back data

        if ((strlen($phone) >= 3)) {

            $phone = stripslashes(trim($_POST['phone']));



            if (empty($phone)) {
                $errors['phone'] = 'Telefon jest wymagany';
            }

            // if there are any errors in our errors array, return a success boolean or false
            if (!empty($errors)) {
                $data['success'] = false;
                $data['errors'] = $errors;
            } else {
                $subject = "Message from $subjectPrefix";
                $body = '
                    Telefon: ' . $phone . '
                ';

                $headers = "MIME-Version: 1.1" . PHP_EOL;
                $headers .= "Content-type: text/html; charset=utf-8" . PHP_EOL;
                $headers .= "Content-Transfer-Encoding: 8bit" . PHP_EOL;
                $headers .= "Date: " . date('r', $_SERVER['REQUEST_TIME']) . PHP_EOL;
                $headers .= "Message-ID: <" . $_SERVER['REQUEST_TIME'] . md5($_SERVER['REQUEST_TIME']) . '@' . $_SERVER['SERVER_NAME'] . '>' . PHP_EOL;
                $headers .= "From: " . "=?UTF-8?B?" . base64_encode($phone) . "?=" . " <$emailTo> " . PHP_EOL;
                $headers .= "Return-Path: $emailTo" . PHP_EOL;
                $headers .= "X-Mailer: PHP/" . phpversion() . PHP_EOL;
                $headers .= "X-Originating-IP: " . $_SERVER['SERVER_ADDR'] . PHP_EOL;

                mail($emailTo, "=?utf-8?B?" . base64_encode($subject) . "?=", $body);

                $data['success'] = true;
                $data['confirmation'] = 'Witamy Twoja wiadomośc została wysłana';
            }

            // return all our data to an AJAX call
//            echo json_encode($data);
        }

    }

    public function bigintval($value) {
        $value = trim($value);
        if (ctype_digit($value)) {
            return $value;
        }
        $value = preg_replace("/[^0-9](.*)$/", '', $value);
        if (ctype_digit($value)) {
            return $value;
        }
        return "CONTACT_FORM_FAILED";
    }
}

