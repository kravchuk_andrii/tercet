<?php
/**
 * Callbacks for Settings API
 *
 * @package tercet
 */

namespace tercet\Api\Callbacks;

/**
 * Settings API Callbacks Class
 */
class SettingsCallback
{
	public function admin_index() 
	{
		return require_once( get_template_directory() . '/views/admin/index.php' );
	}

	public function admin_faq() 
	{
		echo '<div class="wrap"><h1>FAQ Page</h1></div>';
	}

	public function tercet_options_group( $input ) 
	{
		return $input;
	}

	public function tercet_admin_index() 
	{
		echo 'Customize this Theme Settings section and add description and instructions';
	}

	public function tercet_set_main_option()
	{
        $logo_setting = esc_attr( get_option( 'logo_setting' ) );
		echo '<label class="mr-2 h3 text-bold" for="logo_setting">Logo:</label><input type="file" id="logo_setting" name="logo_setting" value="'. $logo_setting .'" placeholder="Logo Setting" />';
		if(isset($logo_setting )){
		   echo '<img src="'. $logo_setting . '">';
        }
	}
}