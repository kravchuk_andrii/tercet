<?php
/**
 * Theme Customizer - Header
 *
 * @package tercet
 */

namespace tercet\Api\Customizer;

use WP_Customize_Color_Control;
use WP_Customize_Control;
use WP_Customize_Image_Control;
use tercet\Api\Customizer;

/**
 * Customizer class
 */
class Header 
{
	/**
	 * register default hooks and actions for WordPress
	 * @return
	 */
	public function register( $wp_customize ) 
	{
		$wp_customize->add_section( 'tercet_header_section' , array(
			'title' => __( 'Header', 'tercet' ),
			'description' => __( 'Customize the Header' ),
			'priority' => 35
		) );

        $wp_customize->add_setting( 'tercet_header_phone' , array(
            'default' => '+48 888 888 888',
            'transport' => 'postMessage', // or refresh if you want the entire page to reload
        ) );

        $wp_customize->add_setting( 'tercet_header_logo' ,  array(
                'default'      => get_template_directory_uri().'/assets/src/img/images/LOGO_NAPOLLO.png',
            )
        );
        $wp_customize->add_setting( 'tercet_header_background_image' ,  array(
                'default' => get_template_directory_uri().'/assets/src/img/images/header-background-image.png',
            )
        );

		$wp_customize->add_setting( 'tercet_header_background_color' , array(
			'default' => '#ffffff',
			'transport' => 'postMessage', // or refresh if you want the entire page to reload
		) );

		$wp_customize->add_setting( 'tercet_header_text_color' , array(
			'default' => '#333333',
			'transport' => 'postMessage', // or refresh if you want the entire page to reload
		) );

		$wp_customize->add_setting( 'tercet_header_link_color' , array(
			'default' => '#004888',
			'transport' => 'postMessage', // or refresh if you want the entire page to reload
		) );

        $wp_customize->add_control( new WP_Customize_Image_Control(  $wp_customize,  'tercet_header_logo',
                array(
                    'label'    => 'Logo',
                    'settings' => 'tercet_header_logo',
                    'section'  => 'tercet_header_section'
                )
            )
        );

        $wp_customize->add_control( new WP_Customize_Image_Control(  $wp_customize,  'tercet_header_background_image',
                array(
                    'label'    => 'Background Image',
                    'settings' => 'tercet_header_background_image',
                    'section'  => 'tercet_header_section'
                )
            )
        );

        $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'tercet_header_phone', array(
            'label' => __( 'Twój telefon', 'tercet' ),
            'section' => 'tercet_header_section',
            'settings' => 'tercet_header_phone',
        ) ) );

        $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'tercet_header_background_color', array(
			'label' => __( 'Header Background Color', 'tercet' ),
			'section' => 'tercet_header_section',
			'settings' => 'tercet_header_background_color',
		) ) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'tercet_header_text_color', array(
			'label' => __( 'Header Text Color', 'tercet' ),
			'section' => 'tercet_header_section',
			'settings' => 'tercet_header_text_color',
		) ) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'tercet_header_link_color', array(
			'label' => __( 'Header Link Color', 'tercet' ),
			'section' => 'tercet_header_section',
			'settings' => 'tercet_header_link_color',
		) ) );

		$wp_customize->get_setting( 'blogname' )->transport = 'postMessage';
        $wp_customize->get_setting( 'blogdescription' )->transport = 'postMessage';

        if ( isset( $wp_customize->selective_refresh ) ) {
			$wp_customize->selective_refresh->add_partial( 'blogname', array(
				'selector' => '.site-title a',
				'render_callback' => function() {
					bloginfo( 'name' );
				},
			) );
			$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
				'selector' => '.site-description',
				'render_callback' => function() {
					bloginfo( 'description' );
				},
			) );
			$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
				'selector' => '#tercet-header-control',
				'render_callback' => function() {
					bloginfo( 'tercet_header_phone' );
				},
			) );
			$wp_customize->selective_refresh->add_partial( 'tercet_header_background_color', array(
				'selector' => '#tercet-header-control',
				'render_callback' => array( $this, 'outputCss' ),
				'fallback_refresh' => true
			) );
			$wp_customize->selective_refresh->add_partial( 'tercet_header_text_color', array(
				'selector' => '#tercet-header-control',
				'render_callback' => array( $this, 'outputCss' ),
				'fallback_refresh' => true
			) );
			$wp_customize->selective_refresh->add_partial( 'tercet_header_link_color', array(
				'selector' => '#tercet-header-control',
				'render_callback' => array( $this, 'outputCss' ),
				'fallback_refresh' => true
			) );
		}
	}

	/**
	 * Generate inline CSS for customizer async reload
	 */
	public function outputCss()
	{
		echo '<style type="text/css">';
			echo Customizer::css( '.site-header', 'background-color', 'tercet_header_background_color' );
			echo Customizer::css( '.site-header', 'color', 'tercet_header_text_color' );
			echo Customizer::css( '.site-header a', 'color', 'tercet_header_link_color' );
		echo '</style>';
	}
}