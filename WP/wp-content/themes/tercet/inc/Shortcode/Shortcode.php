<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018-09-15
 * Time: 23:48
 */


namespace tercet\Shortcode;
use tercet\Shortcode\template\ContactForm;



class Shortcode
{

    public function register()
    {
        add_shortcode( 'contact_form', array($this, 'contact_form_shortcode' ));
    }
    public function contact_form_shortcode(){
        $contact_form = new ContactForm();
        return $contact_form->show_content();
    }

}