<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018-09-15
 * Time: 23:10
 */

namespace tercet\Shortcode\template;

class ContactForm
{
    static private $id = 0;

    public function show_content(){
        $id = self::$id++;
        $admin_ajax = htmlspecialchars(get_template_directory_uri() . '/inc/Custom/Contact.php');
        $contact_form ="";
        $contact_form .= "
        <form id=\"$id\" class=\"contact-form header d-none d-lg-block\" data-url=\"$admin_ajax\">
            <h3 class=\"title\">Wypełnij formularz</h3>
            <h5 class=\"description\">skontaktuj się z biurem sprzedaży</h5>
            <div class=\"row\">
                <div class=\"col\">
                    <input id=\"name\" type=\"text\" class=\"form-control\" placeholder=\"Imię i Nazwisko*\">
                </div>
            </div>
            <div class=\"row\">
                <div class=\"col\">
                    <input id=\"email\" type=\"email\" class=\"form-control\" placeholder=\"Adres email*\">
                </div>
                <div class=\"col\">
                    <input id=\"phone\" type=\"text\" class=\"form-control\" placeholder=\"Telefon*\">
                </div>
            </div>
            <div class=\"row\">
                <div class=\"col\">
                    <textarea id=\"message\" type=\"text\" class=\"form-control\" placeholder=\"Treść wiadomości*\"></textarea>
                </div>
            </div>
            <p class=\"text-blue m-0 p-0\">*pola wymagane</p>
            <div class=\"row\">
                <div class=\"col\">
                    <label for=\"level-$id\" class=\"col-xs-12\"><b>Piętro</b><span
                                class=\"float-right\">od <b class=\"level-min-$id\">1</b> do <b
                                    class=\"level-max-$id\">7</b></span></label>
                    <div class=\"col-xs-12\">
                        <input id=\"level-$id\" type=\"text\" class=\"range-slider\" value=\"\" data-slider-min=\"1\"
                               data-slider-max=\"7\"
                               data-slider-step=\"1\" data-slider-value=\"[1,4]\"/>
                    </div>
                </div>
            </div>
            <div class=\"row\">
                <div class=\"col\">
                    <label for=\"rooms-$id\" class=\"col-xs-12\"><b>Liczba pokoi</b><span
                                class=\"float-right\">od <b class=\"rooms-min-$id\">1</b> do <b
                                    class=\"rooms-max-$id\">4</b></span></label>
                    <div class=\"col-xs-12\">
                        <input id=\"rooms-$id\" type=\"text\" class=\"range-slider\" value=\"\" data-slider-min=\"1\"
                               data-slider-max=\"4\"
                               data-slider-step=\"1\" data-slider-value=\"[1,4]\"/>
                    </div>
                </div>
            </div>
            <div class=\"row\">
                <div class=\"col\">
                    <label for=\"area-$id\" class=\"col-xs-12\"><b>Powierzchnia</b><span
                                class=\"float-right\">od <b class=\"area-min-$id\">29.9</b> do <b
                                    class=\"area-max-$id\">94</b></span></label>
                    <div class=\"col-xs-12\">
                        <input id=\"area - $id\" type=\"text\" class=\"range - slider\" value=\"\" data-slider-min=\"29.46\"
                               data-slider-max=\"96.36\"
                               data-slider-step=\"0.1\" data-slider-value=\"[29.46,96.36]\"/>
                    </div>
                </div>
            </div>
            <div class=\"form-agreement\"></div>

            <button type=\"submit\" class=\"btn btn-primary\">Wyślij formularz</button>

        </form>";

        return $contact_form;
    }
}