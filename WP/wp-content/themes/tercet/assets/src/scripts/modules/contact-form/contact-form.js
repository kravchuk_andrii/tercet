import  agreement from "./contact-form-agreement";
import formSent from "./contack-form-submit";
class ContactForm {

    constructor() {
        this.formAgreement = document.querySelectorAll(".form-agreement");
        this.hlevel  = $("#hlevel");
        this.hrooms  = $("#hrooms");
        this.harea  = $("#harea");
        this.flevel  = $("#flevel");
        this.frooms  = $("#frooms");
        this.farea  = $("#farea");
        this.range  = $(".range-slider");
        this.init();
        this.show();
        this.slide();
        this.shomore();
        formSent();
    }

    init() {
        for (let i = 0; i < this.formAgreement.length; i++) {
            this.formAgreement[i].innerHTML = agreement
        }
        // this.formAgreement.forEach(e => e.innerHTML = agreement);
        this.hlevel.slider({});
        this.hrooms.slider({});
        this.harea.slider({});
        this.flevel.slider({});
        this.frooms.slider({});
        this.farea.slider({});
        this.range.slider({});
    }

    show(){
        let hLvl = this.hlevel.slider('getValue');
        let hRooms = this.hrooms.slider('getValue');
        let hArea = this.harea.slider('getValue');
        let fLvl = this.flevel.slider('getValue');
        let fRooms = this.frooms.slider('getValue');
        let fArea = this.farea.slider('getValue');
        $(".level-header-max").html(hLvl[1]);
        $(".level-header-min").html(hLvl[0]);
        $(".rooms-header-max").html(hRooms[1]);
        $(".rooms-header-min").html(hRooms[0]);
        $(".area-header-max").html(hArea[1]);
        $(".area-header-min").html(hArea[0]);
        $(".level-footer-max").html(fLvl[1]);
        $(".level-footer-min").html(fLvl[0]);
        $(".rooms-footer-max").html(fRooms[1]);
        $(".rooms-footer-min").html(fRooms[0]);
        $(".area-footer-max").html(fArea[1]);
        $(".area-footer-min").html(fArea[0]);
    }

    slide(){
        jQuery( '.slider' ).on('slide, click', () => {
            this.show();
        });
    }

    shomore(){
            // Configure/customize these variables.
            var showChar = 140;  // How many characters are shown by default
            var ellipsestext = "...";
            var moretext = "Rozwiń";
            var lesstext = "Zwiń";


            $('.more').each(function() {
                var content = $(this).html();

                if(content.length > showChar) {

                    var c = content.substr(0, showChar);
                    var h = content.substr(showChar, content.length - showChar);

                    var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

                    $(this).html(html);
                }

            });

            $(".morelink").click(function(){
                if($(this).hasClass("less")) {
                    $(this).removeClass("less");
                    $(this).html(moretext);
                } else {
                    $(this).addClass("less");
                    $(this).html(lesstext);
                }
                $(this).parent().prev().toggle();
                $(this).prev().toggle();
                return false;
            });
    }

}

export default ContactForm;