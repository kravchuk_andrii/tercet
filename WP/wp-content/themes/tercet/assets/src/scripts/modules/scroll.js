import './../../../../node_modules/bootstrap';
class Scroll {

    constructor() {
        this.addScrollTo();
        this.init();
        window.addEventListener('load', () => this.elementActive());
    }

    init() {

        $('#site-navigation a').on('click', function(event) {
            if (this.hash !== '') {
                event.preventDefault();
                // let navHeight = document.getElementById('site-navigation').offsetHeight - 1;
                let navHeight = document.getElementById('masthead').offsetHeight - 1;
                let hash = this.hash;
                if($(hash).length > 0) {
                    $('html, body').animate({
                        scrollTop:  ($(hash).offset().top - navHeight)
                    }, 800);
                }else {
                    window.location = this.href;
                }


            }
        });
    }

    elementActive(){
        // let checkID = this.checkID;
        let scrollTo = document.querySelectorAll('.scroll-to');
        let obj = {};
        for(let i = 0; i < scrollTo.length; i++){
            let el = scrollTo[i];
            obj[el.getAttribute('id')] = el.offsetTop === 0 ? el.parentElement.offsetTop : el.offsetTop;
        }
        let nanElement = "";
        let addNavActive = this.addNavActive;
        window.addEventListener('scroll', ()=> {
            let scrollDistance =  window.pageYOffset + 90;
            let value = Math.max.apply(Math, Object.keys(obj).map(function(e) {
                return obj[e]
            }).filter((x) =>  x <= scrollDistance));
            if(value > 0){
                nanElement = Object.keys(obj).find(key => obj[key] === value);
                addNavActive(nanElement);
            }else {
                let active = document.querySelector('#site-navigation a.active');
                if(active) active.classList.remove('active');
            }

        })

    }

    addNavActive(el){
        let links =  document.querySelectorAll('#site-navigation a');
        for(let i = 0; i < links.length; i++){
            let link = links[i];
                let hash = link.hash;
                if(hash !== '' && hash === `#${el}`) {
                   let active = document.querySelector('#site-navigation a.active');
                   if(active) active.classList.remove('active');
                   link.classList.add('active');
                }
        }
    }

     addScrollTo(){
         let links =  document.querySelectorAll('#site-navigation a');
         for(let i = 0; i < links.length; i++){
             let link = links[i];
             let hash = link.hash;
             if(hash !== '') document.querySelector(hash).classList.add('scroll-to');

         }

    }
}

export default Scroll;