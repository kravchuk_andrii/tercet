class Cookies {
    constructor() {
        this.elementCookies = document.getElementById('cookies');
        this.init();
        this.activateEventListener();
    }
    init(){
        if(this.getCookie("cookiesAccepted") === "true"){
            this.closeCookies();
        }else{
            this.elementCookies.style.display = "block";
        }
    }

    activateEventListener() {
        let that = this;
        let closeBtn = document.querySelectorAll('#cookies .close-cookies');
        for (let i = 0; i < closeBtn.length; i++) {
            closeBtn[i].addEventListener("click", function () {
                that.closeCookies(that)
            });
        }
    }
    getCookie(name) {
        var value = "; " + document.cookie;
        var parts = value.split("; " + name + "=");
        if (parts.length == 2) return parts.pop().split(";").shift();
    }

    setCookie(cname, cvalue) {
        let d = new Date();
        d.setTime(d.getTime() + (5*365*24*60*60*1000));
        let expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    closeCookies(that) {
        this.elementCookies.style.display = "none";
        this.setCookie("cookiesAccepted","true");
    }
}

export default Cookies;