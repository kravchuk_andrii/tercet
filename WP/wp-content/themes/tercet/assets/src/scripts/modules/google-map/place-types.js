let placeTypes = [
    "supermarket",
    "restaurant",
    // "bus_station",
    "transit_station",
    "light_rail_station",
    "subway_station",
    "hospital",
    "local_government_office",
    "school",
    "bicycle_store"
];

let bicycleStore = [
[52.291944, 21.035556],
[52.292417, 21.042028],
[52.291917, 21.048722],
[52.293833, 21.027306],
[52.287806, 21.021389],
[52.292444, 21.056917]
];

let localGovernmentOffice = [
    [52.291303, 21.048989]
];

export {placeTypes, bicycleStore, localGovernmentOffice};


