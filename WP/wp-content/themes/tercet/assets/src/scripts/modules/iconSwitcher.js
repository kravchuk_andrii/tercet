import placeTypes from "./google-map/place-types";
import placeService from "./google-map/places-service";
class IconSwitcher {

    constructor() {
        this.el = document.getElementsByClassName( 'switcher-icon' );
        this.listeners();
    }



    listeners() {
        let elClick = IconSwitcher.elClick;
        for (let i = 0; i <  this.el.length; i++) {
            let el =  this.el[i];
            el.addEventListener("click", function(){elClick(el)});
        }

    }

   static elClick( e ) {
        let imgSrc = e.getElementsByTagName('img')[0].src;
        let p = e.getElementsByTagName('p')[0];
        if(imgSrc.includes("_inactive")){
            e.classList.add('active');
            e.getElementsByTagName('img')[0].src = imgSrc.replace("_inactive","_active");
        }else if(imgSrc.includes("_active")){
            e.classList.remove('active');
            e.getElementsByTagName('img')[0].src = imgSrc.replace("_active","_inactive");
        }
       placeService.toggleGroup(e.getAttribute('data-google'));
    }
}

export default IconSwitcher;