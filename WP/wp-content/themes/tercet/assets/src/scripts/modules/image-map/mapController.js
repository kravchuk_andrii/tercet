class MapController {
    constructor(){
        this.mapContainer =  document.querySelector('.map-container');
        if(!this.mapContainer) return;
        this.mapItems = this.mapContainer.querySelectorAll('.map-item');
        this.app();
    }

    app(){
        [...this.mapItems].map((e) => {
            e.addEventListener('click', () => {
                e.classList.toggle('active');
                this.imageToggle( e.dataset.element )
            })
        })
    }

    imageToggle(el){
        let image = document.querySelector('.image-layer[data-image="'+ el +'"]');
        if(image){
            image.classList.toggle('visible');
        }
    }

}

export default MapController;