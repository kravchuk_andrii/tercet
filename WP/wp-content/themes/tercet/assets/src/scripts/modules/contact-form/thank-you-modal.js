const thankYouModal = `<div class="modal fade" id="thankYouModal" tabindex="-1" role="dialog" aria-labelledby="thankYouModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
    <div class="modal-header">
                <span class="close" data-dismiss="modal" aria-label="Close"><img
                            src="/wp-content/themes/tercet/assets/src/img/images/icon-png/close.png"> </span>
</div>
<div class="modal-body">
<div class="media">
  <img class="align-self-center mr-3" src="/wp-content/themes/tercet/assets/src/img/images/icon-png/envelope.png" alt="Generic placeholder image">
  <div class="media-body">
    <h2 class="mt-0">DZIĘKUJEMY</h2>
    <p>za przesłanie zapytania</p>
  </div>
</div>
</div>
</div>
</div>
</div>`;

export default thankYouModal;