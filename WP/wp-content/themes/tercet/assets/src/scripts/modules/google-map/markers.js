const markers = {
    restaurants: {
        icon:"https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",

    },
    convenience_store: {

    },
    bus_station: {

    },
    transit_station: {

    },
    subway_station: {

    },
    hospital: {

    },
    local_government_office: {

    },
    school: {

    },
    bicycle_store: {

    }
}

export default markers;