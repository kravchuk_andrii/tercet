const AgrementPatern = `<div class="form-check">
                        <input type="checkbox" class="form-check-input" id="agreement1">
                        <label class="form-check-label more" for="agreement1">** 1. Wyrażam zgodę na przetwarzanie przez
                            Napollo
                            Residential Sp. z o.o., podanych przeze mnie w formularzu danych w celu opracowania i udzielenia
                            odpowiedzi
                            na moje pytanie <a target="_blank" href="http://rodo.napollo.pl/"> więcej >></a></label>
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="agreement2">
                        <label class="form-check-label more" for="agreement2">2. Wyrażam zgodę na przetwarzanie przez
                            Napollo
                            Residential Sp. z o.o. podanych przeze mnie w formularzu danych, dla celów działań promocyjnych
                            i
                            marketingowych dotyczących inwestycji mieszkaniowych <a target="_blank" href="http://rodo.napollo.pl/"> więcej >></a></label>
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="agreement3">
                        <label class="form-check-label more" for="agreement3">3. Wyrażam zgodę na otrzymywanie informacji
                            handlowych
                            drogą elektroniczną od Napollo Residential Sp. z o.o. zgodnie z postanowieniami art. 10 ustawy z
                            dnia 18
                            lipca 2012 r. o świadczeniu usług drogą elektroniczną (t.j. Dz.U. z 2017 r. poz. 1219 ) oraz na
                            marketing
                            bezpośredni Napollo Residential Sp. z o.o. przy użyciu telekomunikacyjnych urządzeń końcowych
                            (np.
                            telefon,
                            adres poczty elektronicznej) i automatycznych systemów wywołujących polegający na otrzymywaniu
                            Informacji
                            handlowych o inwestycjach mieszkaniowych <a target="_blank" href="http://rodo.napollo.pl/"> więcej >></a></label>
                    </div>
                    <p class="text-blue">
                        **Wyrażenie zgody jest dobrowolne, ale konieczne do udzielenia odpowiedzi na Państwa zapytanie lub
                        przedstawienie oferty.
                    </p>
`;


export default AgrementPatern;

