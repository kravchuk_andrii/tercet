let mapOptions = {
    center: {
        lat: 52.285460,
        lng:  21.038815,
    },
    zoom: 15,
    disableDefaulteUi: true,
    scrollwheel: false,
    draggable: false,
    minZoom: 14,
    maxZoom: 17
};
export default mapOptions;

