class Carousel {

    constructor() {
        this.close();
    }

    init() {
        // $( '.fade-carousel' ).slick({
        //     dots: true,
        //     arrows: false,
        //     autoplay: true,
        //     infinite: true,
        //     speed: 500,
        //     fade: true,
        //     cssEase: 'linear'
        // });
        // $('#site-navigation').scrollspy({ target: '#site-navigation' });
        //
        // $('#site-navigation a').on('click', function(event) {
        //     if (this.hash !== '') {
        //         event.preventDefault();
        //
        //         let hash = this.hash;
        //
        //         $('html, body').animate({
        //             scrollTop: $(hash).offset().top
        //     }, 800, function() {
        //             window.location.hash = hash;
        //         });
        //     }
        // });
    }

    close(){
        let modals = document.querySelectorAll('.modal-content');
        for(let i = 0; i < modals.length; i++ ){
            let modal = modals[i];
            modal.addEventListener('click', (e)=>{
                if (e.target !== modal) return;
                modal.querySelector('.close').click();

            })
        }
    }
}

export default Carousel;
