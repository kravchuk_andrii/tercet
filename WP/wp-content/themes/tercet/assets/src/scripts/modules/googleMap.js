import mapOptions from "./google-map/map-option.js";
import placesService from "./google-map/places-service";

class GoogleMap {
    constructor() {
        this.element = document.getElementById("map");
        if( this.element !== null){
            this.mapInit();
        }
    }
     async mapInit() {
         if(google === "undefined"){
             setTimeout(this.mapInit(), 800);
         }else{
             let maps = new google.maps.Map(this.element, mapOptions);
             let place = new placesService(mapOptions.center, maps);
         }


    };

}

export default GoogleMap;