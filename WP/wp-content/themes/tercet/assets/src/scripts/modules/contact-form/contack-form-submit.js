import thankYouModal from "./thank-you-modal";

const formSent = () => {
    // $("form").find(".btn").prop("disabled", true);

    if (document.getElementsByTagName("form").length) {
        document.getElementById("colophon").innerHTML += thankYouModal;
    }
    const isValidEmailAddress = (emailAddress) => {
        let pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
        return pattern.test(emailAddress);
    };
    const validatePhone = (txtPhone) => {
        let filter = new RegExp(/^([0-9]|\+|\(|\))*$/);
        return filter.test(txtPhone);
    };
    const formFieldValidation = function (condition, element, text) {
        if (condition) {
            if (!element.next(".invalid-feedback").length) element.after('<div class="invalid-feedback d-block">' + text + '</div>')
        } else {
            element.next(".invalid-feedback").remove();
        }
    };

    $("form input#name").on("change", function () {
        let name = $(this);
        let nameCondition = (name.val() === '' || name.val().length <= 3);
        let nameText = 'Imię i nazwisko muszą mieć więcej niż 3 znaki.';
        formFieldValidation(nameCondition, name, nameText);

    });
    $("form input#email").on("change", function () {
        let email = $(this);
        let emailCondition = (email.val() === '' || !isValidEmailAddress(email.val()));
        let emailText = 'Wpisz poprawny adres e-mail.';
        formFieldValidation(emailCondition, email, emailText);
    });
    $("form input#phone").on("change", function () {
        let phone = $(this);
        let phoneCondition = (phone.val().length < 9 || !validatePhone(phone.val()));
        let phoneText = 'Telefon musi mieć przynajmniej 9 cyfr.';
        formFieldValidation(phoneCondition, phone, phoneText);
    });
    $("form input#message").on("change", function () {
        let message = $(this);
        let messageCondition = (message.val() === '');
        let fieldRequireText = 'To pole jest wymagane';
        formFieldValidation(messageCondition, message, fieldRequireText);
    });

    // $('form.contact-form #agreement1').click(function () {
    //     let father = $(this).parents('.contact-form');
    //     if ($(this).prop('checked')) {
    //         $(father).find(".btn").prop("disabled", false);
    //     } else {
    //         $(father).find(".btn").prop("disabled", true);
    //     }
    // });

    $("form").submit(function (event) {
        event.preventDefault();

        let form = $(this),
            name = form.find('#name').val(),
            email = form.find('#email').val(),
            phone = form.find('#phone').val(),
            message = form.find('#message').val(),
            agreement1 = form.find('#agreement1').is(":checked"),
            agreement2 = form.find('#agreement2').is(":checked"),
            agreement3 = form.find('#agreement3').is(":checked"),
            hlevel = form.find("#hlevel").val(),
            hrooms = form.find("#hrooms").val(),
            harea = form.find("#harea").val(),
            flevel = form.find("#flevel").val(),
            frooms = form.find("#frooms").val(),
            farea = form.find("#farea").val(),
            ajaxurl = form.data('url'),
            action = 'tercet_save_user_contact_form';
        if (name !== undefined) {
            if (name === '' || name.length <= 3) {
                if (!form.find('#name').next(".invalid-feedback").length) {
                    form.find('#name').after('<div class="invalid-feedback d-block">Imię i nazwisko muszą mieć więcej niż 3 znaki.</div>');
                }
                return;
            }
            if (email === '' || !isValidEmailAddress(email)) {
                if (!form.find('#email').next(".invalid-feedback").length) {
                    form.find('#email').after('<div class="invalid-feedback d-block">Wpisz poprawny adres e-mail.</div>');
                }
                return;
            }

            if (message === '') {
                if (!form.find("#message").next(".invalid-feedback").length) {
                    form.find("#message").after('<div class="invalid-feedback d-block">To pole jest wymagane</div>');
                }
                return;
            }
        }
        if (phone.length < 9 || !validatePhone(phone)) {
            if (!form.find("#phone").next(".invalid-feedback").length) {
                form.find("#phone").after('<div class="invalid-feedback d-block">Telefon musi mieć przynajmniej 9 cyfr.</div>');
            }
            return;
        }
        if (!agreement1) {
            if (!form.find("#agreement1").next(".invalid-feedback").length) {
                form.find("#agreement1").after('<div class="invalid-feedback d-block">Prosimy o wyrażenie zgody</div>');
            }
            return
        }

        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: {
                name,
                email,
                phone,
                message,
                hlevel,
                hrooms,
                harea,
                flevel,
                frooms,
                farea,
                agreement1,
                agreement2,
                agreement3,
                action
            },

            error: function (response) {
                jQuery('#orderCall').modal('hide');
                setTimeout(function () {
                    jQuery("#thankYouModal").find(".media-body").html(`<h2 class="mt-0">PRZEPRASZAMY</h2>
                                <p>Wysłanie formularzu nie powiodło się.<br>
                                Prosimy odswieżyć stronę i spróbować jeszcze raz</p>`);
                    jQuery("#thankYouModal").modal("show");
                }, 500);
            },
            success: function (response) {
                jQuery('#orderCall').modal('hide');
                if (response == "sent") {
                    setTimeout(function () {
                        jQuery("#thankYouModal").find(".media-body").html(`<h2 class="mt-0">DZIĘKUJEMY</h2>
                                <p>za przesłanie zapytania</p>`);
                        jQuery("#thankYouModal").modal("show");

                    }, 500);

                } else {
                    setTimeout(function () {
                        jQuery("#thankYouModal").find(".media-body").html(`<h2 class="mt-0">PRZEPRASZAMY</h2>
                                <p>Wysłanie formularzu nie powiodło się.<br>
                                Prosimy odswieżyć stronę i spróbować jeszcze raz</p>`);
                        jQuery("#thankYouModal").modal("show");
                    }, 500);
                }

            }

        });
    })
};

export default formSent;