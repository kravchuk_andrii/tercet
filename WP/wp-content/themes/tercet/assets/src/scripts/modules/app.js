class App {

	constructor() {
		this.el = document.querySelector( '.el' );
		this.imageSection = document.getElementById("image-section");
		this.headerForm = document.querySelector( '.contact-form.header');
		this.firstSection = document.querySelector( '#image-section .first-section');
        this.brandLogo = document.querySelector(".tercet-logo");
        this.fixedLogo = document.getElementById("fixed-logo");
        this.brandLogo.style.opacity = "1";
        this.fixedLogo.style.opacity = "1";
		this.scrollAction();
        this.fillHeader();
		this.listeners();
		this.init();
		this.hamburger();
		this.redirect();
	}

	init() {

	}

	scrollAction(){
		window.onscroll = () => {
          this.fillHeader();
		}
	}
	redirect(){
	    let page, build;
        page = document.getElementsByClassName("page-id-2")[0];
        if(page){

            build = page.querySelector('a[href="#gallery-0-1"]');
            build.href = "/budowa";
        }
    }

	fillHeader(){
        let element = document.getElementById("inwestycja");
        let buttons = document.querySelector("#masthead .button-box");
        let sectionDistance = element ? (element.offsetTop - buttons.offsetHeight): buttons.offsetTop;

        let winPosition = (-1) * window.pageYOffset;
        this.brandLogo.style.transform = 'translate3d(0, ' + winPosition +  'px, 0)';

        if (window.pageYOffset > sectionDistance) {
            buttons.classList.add("sticky");
        }
        if(window.pageYOffset < 50){
            buttons.classList.remove("sticky");
        }
	}

	hamburger(){
		let hamburger  =  document.getElementById("hamburger");
        for (let i = 0; i < 6 ; i++) {
            hamburger.appendChild(document.createElement('span'));
        }
		hamburger.addEventListener('click', function(){
			this.classList.toggle("open");
		})
	}

	listeners() {
		if ( this.el ) {
			this.el.addEventListener( 'click', this.elClick );
		}

	}

	elClick( e ) {
		e.target.classList.add( 'text-light-grey' );
		e.target.addEventListener( 'transitionend', ( e ) => ( 'color' === e.propertyName ) ? e.target.classList.remove( 'text-light-grey' ) : '' );
	}

}

export default App;
