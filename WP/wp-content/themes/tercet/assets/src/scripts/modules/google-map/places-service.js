import {placeTypes, bicycleStore, localGovernmentOffice} from "./place-types";

class PlaceService {
    constructor(center, map) {
        PlaceService.icons = document.querySelectorAll('.switcher-icon');
        PlaceService.map = map;
        PlaceService.center = center;
        PlaceService.markersArray = {
            "supermarket": [],
            "restaurant": [],
            "bus_station": [],
            "transit_station": [],
            "light_rail_station": [],
            "subway_station": [],
            "hospital": [],
            "local_government_office": [],
            "school": [],
            "bicycle_store": []
        };
        PlaceService.marker;
        PlaceService.infoWindow = new google.maps.InfoWindow();
        PlaceService.service = new google.maps.places.PlacesService(PlaceService.map);
        this.initialize();
        setTimeout(() => PlaceService.hideMarkers(), 2000);
    }



    static getLetteredIcon(letter) {
        return {
            url: "wp-content/themes/tercet/assets/src/img/google_maps/" + letter + ".svg",
            scaledSize: new google.maps.Size(50, 50), // scaled size
            origin: new google.maps.Point(0,0), // origin
            anchor: new google.maps.Point(0, 0) // anchor
        }
    }

    initialize() {
        let myLatlng = new google.maps.LatLng(PlaceService.center.lat, PlaceService.center.lng);

        let tercetMark = {
            url: 'wp-content/themes/tercet/assets/src/img/google_maps/lokalizcja_googlemaps.svg',
            scaledSize: new google.maps.Size(80, 80),
            origin: new google.maps.Point(0,0),
            anchor: new google.maps.Point(0, 0)};
        let marker = new google.maps.Marker({
            position: myLatlng,
            map: PlaceService.map,
            animation: google.maps.Animation.DROP,
            icon: tercetMark
        });
        google.maps.event.addListenerOnce(PlaceService.map, 'bounds_changed', function (){PlaceService.performSearch(placeTypes)});
        PlaceService.infoWindow.setContent('<h3>Tercet</h3>');
        PlaceService.infoWindow.open(PlaceService.map, marker);


    }

    static performSearch(place) {
        for (let i = 0; i < place.length; i++) {
            PlaceService.performTypeSearch(place[i], PlaceService.getLetteredIcon(place[i]));
        }
    }

    static performTypeSearch(type, icon) {
        if(type === 'bicycle_store'){
            PlaceService.customMarker('bicycle_store', bicycleStore, icon);
            return;
        }
        if(type === 'local_government_office'){
            PlaceService.customMarker('local_government_office', localGovernmentOffice, icon);
            return;
        }

        PlaceService.service.nearbySearch(
            {location: PlaceService.center, radius: 900, type},
            function(results, status) {
            if (status !== 'OK') return;
            for (let i = 0, result; result = results[i]; i++) {
                PlaceService.createMarker(result, icon, type);
            }
        });


    }

    static createMarker(place, icon, type) {
        PlaceService.marker = new google.maps.Marker({
            map: PlaceService.map,
            position: place.geometry.location,
            icon: icon,
            animation: google.maps.Animation.DROP,

        });

        if (!PlaceService.markersArray[type]) PlaceService.markersArray[type] = [];

        PlaceService.markersArray[type].push(PlaceService.marker);


        google.maps.event.addListener(PlaceService.marker, 'click', function () {
            PlaceService.service.getDetails(place, function (result, status) {
                if (status !== 'OK') {  return;  }
                PlaceService.infoWindow.setContent(result.name);
                PlaceService.infoWindow.open(PlaceService.map, PlaceService.marker);
            });
        });

    }
    static customMarker(type, coordinates, icon){
        let center = {};
        let marker;
        for (let i = 0; i < coordinates.length; i++) {
          marker =  new google.maps.Marker({
                map: PlaceService.map,
                position: new google.maps.LatLng(coordinates[i][0], coordinates[i][1]),
                icon: icon,
                animation: google.maps.Animation.DROP,

            });
            PlaceService.markersArray[type].push(marker);

        }
    }

    static toggleGroup(type){
        for (let i = 0; i < PlaceService.markersArray[type].length; i++) {
            let marker = PlaceService.markersArray[type][i];
            if(!marker.getVisible()){
                marker.setVisible(true);
            }else{
                marker.setVisible(false);
            }
        }
    }
     static hideMarkers(){
        for (let i = 0; i < PlaceService.icons.length; i++) {
            let el = PlaceService.icons[i];
            if(!el.classList.contains("active")){
                PlaceService.toggleGroup(el.getAttribute('data-google'));
            }
        }
    }
}

export default PlaceService;