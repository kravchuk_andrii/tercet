/**
 * Manage global libraries like jQuery or THREE from the package.json file
 */

// Import libraries
import 'jquery';
import 'popper.js';
import 'slick-carousel';
import 'bootstrap-slider';
import 'readmore-js/readmore';
import 'bootstrap';
import './modules/polyfill';

// Import custom modules
import App from'./modules/app.js';
import Cookies from './modules/cookies';
import Carousel from './modules/carousel.js';
import Scroll from './modules/scroll';
import GoogleMap from './modules/googleMap.js';
import ContactForm from "./modules/contact-form/contact-form.js";
import IconSwitcher from "./modules/iconSwitcher";
import MapController from './modules/image-map/mapController';

const app = new App();
const cookies = new Cookies();
const carousel = new Carousel();
const scroll = new Scroll();
const googleMap = new GoogleMap();
// const slider = new Slider();
const contact = new ContactForm();
const iconSwitcher = new IconSwitcher();
const mapController = new MapController();