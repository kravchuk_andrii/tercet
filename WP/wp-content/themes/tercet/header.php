<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package tercet
 */
$tercet_header_background_image = get_theme_mod('tercet_header_background_image');
$tercet_header_logo = get_theme_mod('tercet_header_logo');
$tercet_header_phone = get_theme_mod('tercet_header_phone');


?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
<!--    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, user-scalable=no">-->
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <link rel="shortcut icon" href="<?php echo get_bloginfo( 'template_directory' );?>/assets/src/img/images/favicon/tercet_favicon_16x16.png" type="image/x-icon" />

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?> data-spy="scroll" data-target="#site-navigation" data-offset="122">
<div id="page" class="site" <?php echo !is_customize_preview() ?: ''; ?>>
    <header id="masthead" class="site-header" role="banner"
        <?php
        if ($tercet_header_background_image) :
            echo 'style="background-image: url(\' ' . $tercet_header_background_image . ' \');
                background-size: cover;"';
        endif; ?>>

        <?php if (is_customize_preview()) echo '<div id="tercet-header-control"></div>'; ?>

        <div class="container-narrow">

            <div class="row">
                <div class="col-xs-12 site">

                    <?php
                    if (has_nav_menu('primary')) :
                        ?>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#site-navigation"
                                aria-controls="site-navigation" aria-expanded="false" aria-label="Toggle navigation">
                            <div id="hamburger"></div>
                        </button>

                            <nav id="site-navigation" class="main-navigation col-xl-5 collapse" role="navigation">

                                <?php
                                wp_nav_menu(array(
                                    'theme_location' => 'primary',
                                    'menu_id' => 'primary-menu',
                                    'walker' => new tercet\Core\WalkerNav(),
                                ));
                                ?>
                            </nav>

                    <?php
                    endif;
                    ?>
                    <div class="col m-auto text-center position-relative logo-box">
                        <img id="fixed-logo" class="animated"  src="<?php echo get_template_directory_uri() ?>/assets/src/img/images/tercet-header-logo.png" alt="tercet header logo">
                        <img class="shadow tercet-logo animated" src="<?php echo get_template_directory_uri() ?>/assets/src/img/images/tercet-main.jpeg">
                    </div>

                    <div class="col site-branding">
                        <img src="<?php
                        if ($tercet_header_logo) :
                            echo $tercet_header_logo;
                        else:
                            echo get_template_directory_uri() . '/assets/src/img/images/LOGO_NAPOLLO.png';
                        endif; ?>" alt="Logo Napollo">
                    </div><!-- .site-branding -->

                    <?php
                    if ($tercet_header_phone) :
                        ?>
                    <div class="phone col">
                        <a class="i-phone" href="tel:<?php echo (int)filter_var($tercet_header_phone, FILTER_SANITIZE_NUMBER_INT) ?>"><?php
                            echo $tercet_header_phone
                            ?></a>
                    </div>

                    <?php endif ?>

                    <div class="button-box">
                        <a target="_blank" href="http://ftp.napinvest.com.pl/nap/Default.aspx?Kind=14&ID=17&Code=DOM%20POD%20SZ%C3%93STK%C4%84" class="i-plan-mieszkan target-button"><span
                                    class="text-white text-uppercase">Plany<br>mieszkań</span></a>
                        <a href="#" class="i-phone target-button" data-toggle="modal" data-target="#orderCall"><span
                                    class="text-white text-uppercase" href="#">Zamów<br>rozmowę</span></a>
                    </div>
                </div><!-- .col -->
            </div><!-- .row -->
        </div><!-- .container-fluid -->

    </header><!-- #masthead -->

    <div id="head-spacer"></div>

    <div class="modal fade" id="orderCall" tabindex="-1" role="dialog" aria-labelledby="orderCall" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <span class="close" data-dismiss="modal" aria-label="Close"><img
                            src="<?php echo get_template_directory_uri() ?>/assets/src/img/images/icon-png/close.png"> </span>
                </div>
                <div class="modal-body">
                    <h2 class="modal-title">PROSZĘ PODAĆ NUMER TELEFONU</h2>
                    <h3 class="modal-description">nasz konsultant oddzwoni do Państwa</h3>
                    <form data-url="<?php echo $admin_ajax ?>">
                        <div class="form-group">
                            <input type="text" class="form-control" id="phone" aria-describedby="Phone"
                                   placeholder="Numer telefonu*">
                        </div>
                        <div class="form-agreement"></div>
                        <div class="text-center"><button type="submit" class="btn btn-secondary">Wyślij</button></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div id="content" class="site-content">
        <div id="cookies">
            <div class="row">
                <div class="i-cookies h3 text-blue col-xs-12"><b>COOKIES</b><span><img class="float-right close-cookies"
                                                                                       src="<?php echo get_template_directory_uri() ?>/assets/src/img/images/icon-png/close.png" </span>
                </div>
                <p>
                    Ta strona wykorzystuje pliki cookies. Używamy
                    informacji zapisanych za pomocą plików cookies
                    m.in. w celach reklamowych, statystycznych i w
                    celu dostosowania naszej strony do indywidualnych
                    potrzeb użytkowników. Korzystanie z naszej
                    strony bez zmiany ustawień dotyczących cookies
                    oznacza, że będą one zapisane w pamięci
                    urządzenia. Korzystając ze strony wyrażasz zgodę
                    na używanie plików cookies, zgodnie z aktualnymi
                    ustawieniami przeglądarki. <a href="#">Polityka prywatności</a>
                </p>
                <button class="btn-outline-blue close-cookies">Zgadzam się</button>
            </div>
        </div>