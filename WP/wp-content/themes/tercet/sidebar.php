<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package tercet
 */

if ( ! is_active_sidebar( 'tercet-sidebar' ) ) :
	return;
endif;
?>

<?php if ( is_customize_preview() ) echo '<div id="tercet-sidebar-control"></div>'; ?>

<aside id="secondary" class="widget-area" role="complementary">
	<?php dynamic_sidebar( 'tercet-sidebar' ); ?>
</aside><!-- #secondary -->
