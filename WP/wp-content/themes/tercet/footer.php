<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package tercet
 */

?>

</div><!-- #content -->

<?php if (is_customize_preview()) echo '<div id="tercet-footer-control" style="margin-top:-30px;position:absolute;"></div>'; ?>

<footer id="colophon" class="container-narrow site-footer  p-0 justify-content-center bg-image" role="contentinfo"
        style="background-image: url('<?php echo get_template_directory_uri() ?>/assets/src/img/images/background/map-background.png')">
    <div class="container  p-0 justify-content-center">
        <div class="footer-container"
        >
            <div class="site-branding">
                <img src="<?php echo get_template_directory_uri() ?>/assets/src/img/images/LOGO_NAPOLLO.png">
            </div>
            <div class="responsive-separator mob"></div>
            <h3 class="text-white"><b class="text-uppercase">Biuro sprzedaży</b><br>
                ul.&nbsp;Poborzańska&nbsp;39,&nbsp;lok.&nbsp;1, 03&nbsp;-&nbsp;368&nbsp;Warszawa</h3>
            <div class="responsive-separator mob"></div>
            <div class="policy">
                <a class="h3 text-uppercase text-white" href="#"> <b>Polityka</b><br>prywatności</a>
            </div>
            <div class="responsive-separator"></div>
            <?php
            $tercet_header_phone = get_theme_mod('tercet_header_phone');
            if ($tercet_header_phone) :
                ?>
                <div class="phone h2 i-phone">
                    <a class="text-white"
                       href="tel:222741809">22 274 18 09    </a>
                </div>
            <?php endif ?>
            <div class="responsive-separator"></div>
            <div class="d-flex align-items-center">
                <img src="<?php echo get_template_directory_uri() ?>/assets/src/img/images/PZFD_LOGO.png">
            </div>

        </div>
    </div>
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
