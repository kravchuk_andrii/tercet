<?php
/**
 * Podstawowa konfiguracja WordPressa.
 *
 * Ten plik zawiera konfiguracje: ustawień MySQL-a, prefiksu tabel
 * w bazie danych, tajnych kluczy, używanej lokalizacji WordPressa
 * i ABSPATH. Więćej informacji znajduje się na stronie
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Kodeksu. Ustawienia MySQL-a możesz zdobyć
 * od administratora Twojego serwera.
 *
 * Ten plik jest używany przez skrypt automatycznie tworzący plik
 * wp-config.php podczas instalacji. Nie musisz korzystać z tego
 * skryptu, możesz po prostu skopiować ten plik, nazwać go
 * "wp-config.php" i wprowadzić do niego odpowiednie wartości.
 *
 * @package WordPress
 */

// ** Ustawienia MySQL-a - możesz uzyskać je od administratora Twojego serwera ** //
/** Nazwa bazy danych, której używać ma WordPress */
define('DB_NAME', '01398324_word');

/** Nazwa użytkownika bazy danych MySQL */
define('DB_USER', '01398324_word');

/** Hasło użytkownika bazy danych MySQL */
define('DB_PASSWORD', 'loiasp32');

/** Nazwa hosta serwera MySQL */
define('DB_HOST', 'localhost');

/** Kodowanie bazy danych używane do stworzenia tabel w bazie danych. */
define('DB_CHARSET', 'utf8');

/** Typ porównań w bazie danych. Nie zmieniaj tego ustawienia, jeśli masz jakieś wątpliwości. */
define('DB_COLLATE', '');

/**#@+
 * Unikatowe klucze uwierzytelniania i sole.
 *
 * Zmień każdy klucz tak, aby był inną, unikatową frazą!
 * Możesz wygenerować klucze przy pomocy {@link https://api.wordpress.org/secret-key/1.1/salt/ serwisu generującego tajne klucze witryny WordPress.org}
 * Klucze te mogą zostać zmienione w dowolnej chwili, aby uczynić nieważnymi wszelkie istniejące ciasteczka. Uczynienie tego zmusi wszystkich użytkowników do ponownego zalogowania się.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Q+2})G*,#|&q_*uua=^X,j3|J4y]p&yuNGfn[m~|rrUC[`_=;tfA|T-cZ5WI6Loj');
define('SECURE_AUTH_KEY',  'b>JJ+G(C>:c|g#*KX:/d-l6A ^CTGR-8EXU3~>6=gI/5&=  1U>St)c4(jwI`P#A');
define('LOGGED_IN_KEY',    '3[NB|@c;k>|1Gbk2QykW@Ho7`8Ut/|k>oJI=|TsBR{8?o}Mdn390RzA6r,+k3e;|');
define('NONCE_KEY',        'z}tBJ6D:%^) pr1-xhQwZrZbEp0xGeC#DprnJ?tW2/tMWRt@zubYtL>.Uu=(,([d');
define('AUTH_SALT',        '|W9 AYF7cbD3Rs;Xf|h @eZ;t*<qO1.%pW]Q%2l-cpOF&zP#nB%!}`/u*f2LGG>O');
define('SECURE_AUTH_SALT', 'I}6N|3y+-AXZ wJl-^~cdM`6s>.+4uz`M[z3@$?@-4}A,nBC=|W`zO1q%!p^ES>n');
define('LOGGED_IN_SALT',   'o+XN,_wfJsD]!>6^}Q=JH6[Bo1F%U?2QULb<iIN1&.PfjP>rE)=^<lU0229%9PyK');
define('NONCE_SALT',       ' mC-No[4x6qWOe)ynbj{kp-]:B>8s+gX:ofyUG6HIU,q~tbch7rwS_u%b#!msi43');

/**#@-*/

/**
 * Prefiks tabel WordPressa w bazie danych.
 *
 * Możesz posiadać kilka instalacji WordPressa w jednej bazie danych,
 * jeżeli nadasz każdej z nich unikalny prefiks.
 * Tylko cyfry, litery i znaki podkreślenia, proszę!
 */
$table_prefix  = 'wp_';

/**
 * Kod lokalizacji WordPressa, domyślnie: angielska.
 *
 * Zmień to ustawienie, aby włączyć tłumaczenie WordPressa.
 * Odpowiedni plik MO z tłumaczeniem na wybrany język musi
 * zostać zainstalowany do katalogu wp-content/languages.
 * Na przykład: zainstaluj plik de_DE.mo do katalogu
 * wp-content/languages i ustaw WPLANG na 'de_DE', aby aktywować
 * obsługę języka niemieckiego.
 */
define('WPLANG', 'en_EN');


/* Multisite */
define( 'WP_ALLOW_MULTISITE', true );


define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', 'wordpress.4deco.pl');
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);

/**
 * Dla programistów: tryb debugowania WordPressa.
 *
 * Zmień wartość tej stałej na true, aby włączyć wyświetlanie ostrzeżeń
 * podczas modyfikowania kodu WordPressa.
 * Wielce zalecane jest, aby twórcy wtyczek oraz motywów używali
 * WP_DEBUG w miejscach pracy nad nimi.
 */
define('WP_DEBUG', false);

/* To wszystko, zakończ edycję w tym miejscu! Miłego blogowania! */

/** Absolutna ścieżka do katalogu WordPressa. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Ustawia zmienne WordPressa i dołączane pliki. */
require_once(ABSPATH . 'wp-settings.php');
